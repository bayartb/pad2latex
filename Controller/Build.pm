package Pad2latex::Controller::Build;

use strict;
use warnings;
use base 'Catalyst::Controller';

use Pad2latex::Sql::Basic;
use Pad2latex::Engine::Build;
use Pad2latex::Engine::Path;

### sub edit :Local {
### 	my ( $self, $c ) = @_;
### 	my $vars = {};
### 	my $fid;
### 	if ( $c->req->param('id') ) {
### 		$fid = $c->req->param('id');
### 		$c->log->info("On edite un folder $fid");
### 		my $folder = folder_par_id($c->req->param('id'));
### 		if ( $folder ) {
### 			$c->log->info("Je trouve le folder.");
### 			$vars->{'folder'} = $folder;
### 		} else {
### 			$c->log->error("Folder '$fid' introuvable");
### 			$fid = undef;
### 		}
### 	}
### 	$vars->{'folders'} = list_folders();
### 
### 	if ( $c->req->param('submit') ) {
### 		my $name = $c->req->param('name');
### 		my $par  = $c->req->param('parent');
### 		if ( $vars->{'folder'} ) {
### 			$c->log->info("Mise a jour du folder '$fid' avec name='$name' et parent='$par'");
### 			set_folder_name($fid, $name);
### 			set_folder_par($fid, $par);
### 		} else {
### 			$c->log->info("Creation du folder '$fid' avec name='$name' et parent='$par'");
### 			$fid = new_folder($name, $par);
### 		}
### 		$c->res->redirect(abs_path('/'));
### 		return;
### 	}
### 	$c->detach('TT','final', [ 'folder_edit', $vars ]);
### }

sub get_pdf :Local {
	my ( $self, $c ) = @_;
	my $vars = {};
	$vars->{'folders'} = list_folders();
	my $bid = $c->req->param('id') // -1;
	my $build = build_par_id($bid);
	if ( ! $build ) {
		...;
	}
	if ( $build->{'BUILD_STATE'} !~ /^(success|warning)$/ ) {
		...;
	}
	my $dir1 = prepare_build_directory($bid);
	my $dir2 = prepare_store_directory($bid);
	if ( -f $dir1.'/main.pdf' && ! -f $dir2.'/main.pdf' ) {
		`cp $dir1/main.pdf $dir2/main.pdf`;
	}
	if ( ! -f $dir2.'/main.pdf' ) {
		...;
	}
	my $filehandle;
	open $filehandle, '<', "$dir2/main.pdf";
	$c->res->header('Content-Disposition' => "attachment; filename=main.pdf");
	$c->res->body($filehandle);
}

sub do_view :Private {
	my ( $self, $c, $bid, $todo ) = @_;
	my $vars = {};
	$vars->{'folders'} = list_folders();
	my $build = build_par_id($bid);
	if ( ! $build ) {
		...;
	}
	my $doc = document_par_id($build->{'DOCUMENT_ID'});
	if ( ! $doc ) {
		...;
	}

	if ( $build->{'BUILD_STATE'} eq 'building' ) {
		if ( ! check_build_process($bid) ) {
			# Need to reload, it is now 'failed'
			$build = build_par_id($bid);
		}
	}
	$vars->{'build'} = $build;
	$vars->{'document'} = $doc;
	$vars->{'has_tex_log'} = build_has_tex_log($bid);
	$vars->{'has_my_log'}  = build_has_my_log($bid);
	if ( $todo eq 'tex' ) {
		$vars->{'log'} = build_get_tex_log($bid);
	} elsif ( $todo eq 'my' ) {
		$vars->{'log'} = build_get_my_log($bid);
	} elsif ( $todo eq 'synth' ) {
		$vars->{'log'} = build_get_synth_log($bid);
	}
	$c->detach('TT', 'final', [ 'build_view', $vars ]);
}

sub view :Local {
	my ( $self, $c ) = @_;
	my $bid = $c->req->param('id') // -1;
	$self->do_view($c, $bid, undef);
}

sub view_tex_log :Local {
	my ( $self, $c ) = @_;
	my $bid = $c->req->param('id') // -1;
	$self->do_view($c, $bid, 'tex');
}

sub view_synth_log :Local {
	my ( $self, $c ) = @_;
	my $bid = $c->req->param('id') // -1;
	$self->do_view($c, $bid, 'synth');
}

sub view_my_log :Local {
	my ( $self, $c ) = @_;
	my $bid = $c->req->param('id') // -1;
	$self->do_view($c, $bid, 'my');
}

sub recent :Local {
	my ( $self, $c ) = @_;
	my $vars = {};
	$vars->{'folders'} = list_folders();
	my $list = [ build_recent() ];
	for my $l ( @$list ) {
		$l->{'document'} = document_par_id($l->{'DOCUMENT_ID'});
		$l->{'folder'} = folder_par_id($l->{'document'}->{'FOLDER_ID'});
	}
	$vars->{'list_builds'} = $list;
	$c->detach('TT','final', [ 'build_recent', $vars ]);
}


1;
