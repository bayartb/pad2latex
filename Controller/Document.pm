package Pad2latex::Controller::Document;

use strict;
use warnings;
use base 'Catalyst::Controller';

use POSIX qw(getppid);
use FDN::Sql::Basic qw(dodate);
use Pad2latex::Sql::Basic;
use Pad2latex::Engine::Build;
use Pad2latex::Engine::Path;
use Pad2latex::Engine::Pad;


sub edit :Local {
	my ( $self, $c ) = @_;
	my $vars = {};
	my $did;
	if ( $c->req->param('id') ) {
		$did = $c->req->param('id');
		$c->log->info("On edite un document $did");
		my $document = document_par_id($c->req->param('id'));
		if ( $document ) {
			$c->log->info("Je trouve le document.");
			$vars->{'document'} = $document;
		} else {
			$c->log->error("document '$did' introuvable");
			$did = undef;
		}
	}
	$vars->{'pfs'} = list_pad_pfs();
	$vars->{'folders'} = list_folders();

	if ( $c->req->param('submit') ) {
		my $name = $c->req->param('name');
		my $fid  = $c->req->param('folder');
		my $pf   = $c->req->param('pf');
		my $pad  = $c->req->param('pad');
		my $type = $c->req->param('type');
		my $state = $c->req->param('state');
		if ( $vars->{'document'} ) {
			$c->log->info("Mise a jour du document '$did' avec name='$name' et parent='$fid'");
			set_document_name($did, $name);
			set_document_folder_id($did, $fid);
			set_document_pad($did, $pad);
			set_document_pf($did, $pf);
			set_document_dtmod($did, 'now');
		} else {
			$did = new_document($name, $fid, $pf, $pad);
			$c->log->info("Creation du document '$did' avec name='$name' et parent='$fid'");
			set_document_dtin($did, 'now');
			set_document_dtmod($did, 'now');
		}
		set_document_type($did, $type);
		set_document_state($did, $state);
		$c->res->redirect(abs_path('/document/check?id='.$did));
		return;
	}
	$c->detach('TT','final', [ 'document_edit', $vars ]);
}

sub view :Local {
	my ( $self, $c ) = @_;
	my $did = $c->req->param('id');
	my $doc = document_par_id($did);
	if ( ! $doc ) {
		$c->res->redirect(abs_path('/'));
		return;
	}
	$doc->{'pf_name'} = pf_name($doc);
	my $vars = {};
	$vars->{'folders'} = list_folders();
	$vars->{'document'} = $doc;
	$vars->{'list_builds'} = [ build_par_document_id_sorted($did) ];
	if ( $doc->{'FOLDER_ID'} > 0 ) {
		$vars->{'folder'} = folder_par_id($doc->{'FOLDER_ID'});
	} else {
		$vars->{'folder'} = { FOLDER_ID => -1, FOLDER_NAME => 'Racine' };
	}
	$c->detach('TT','final', [ 'document_view', $vars ]);
}

sub check :Local {
	my ( $self, $c ) = @_;
	my $did = $c->req->param('id');
	my $doc = document_par_id($did);
	if ( ! $doc ) {
		$c->res->redirect(abs_path('/'));
		return;
	}
	my $vars = {};
	$vars->{'folders'} = list_folders();
	$vars->{'document'} = $doc;
	my $url = $doc->{'DOCUMENT_PAD'};
	$vars->{'test'} = {};
	Pad2latex::Engine::Pad::check_url($doc, $vars->{'test'});
	$c->detach('TT','final', [ 'document_check', $vars ]);
}

sub recent :Local {
	my ( $self, $c ) = @_;
	my $vars = {};
	$vars->{'folders'} = list_folders();
	my $list = [ document_recent() ];
	for my $l ( @$list ) {
		$l->{'folder'} = folder_par_id($l->{'FOLDER_ID'});
	}
	$vars->{'list_documents'} = $list;
	$c->detach('TT','final', [ 'document_recent', $vars ]);
}

sub build :Local {
	my ( $self, $c ) = @_;
	my $did = $c->req->param('id');
	my $doc = document_par_id($did);
	if ( ! $doc ) {
		$c->res->redirect(abs_path('/'));
		return;
	}

	my $bid = new_build($did, $c->session->{'user_name'} // 'Anonymous');
	set_build_state($bid, 'new');
	set_build_dtin($bid, 'now');
	set_build_dtmod($bid, 'now');
	set_document_dtmod($did, 'now');
	my $dir = prepare_build_directory($bid);
	if ( ! $dir ) {
		set_build_state($bid, 'failed');
		set_build_err($bid,'Failed to create the directories where I was supposed to build the document.');
		$c->res->redirect(abs_path('/build/view?id='.$bid));
		return;
	}
	if ( ! fetch_pads($doc, $dir) ) {
		set_build_state($bid, 'failed');
		set_build_err($bid,'Failed to fetch the text from the pads. Please run a "Check" on the document.');
		$c->res->redirect(abs_path('/build/view?id='.$bid));
		return;
	}
	set_build_state($bid, 'data');
	my $pid = fork();
	if ( $pid < 0 ) {
		set_build_state($bid, 'failed');
		set_build_err($bid, 'Failed to fork a new process. Cannot do anything. Plese consult an adminsys, or event better, a wizzard.');
		$c->res->redirect(abs_path('/build/view?id='.$bid));
		return;
	}
	if ( ! $pid ) {
		# Fiston
		print STDERR "Je suis $$, lance par ".getppid()." et je devrais compiler un truc.\n";
		exec("/usr/local/bin/pad2latex-builder.pl", $bid) or die "Failed to launch : $@";
		exit(0);
	}
	$c->log->info("Launched process $pid, which is in charge or building $bid");
	set_build_state($bid, 'building');
	set_build_process($bid, $pid);
	set_build_dtstart($bid, 'now');

	$c->res->redirect(abs_path('/build/view?id='.$bid));
}

1;
