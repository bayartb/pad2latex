package Pad2latex::Controller::Folder;

use strict;
use warnings;
use base 'Catalyst::Controller';

use Pad2latex::Sql::Basic;
use Pad2latex::Engine::Path;

sub edit :Local {
	my ( $self, $c ) = @_;
	my $vars = {};
	my $fid;
	if ( $c->req->param('id') ) {
		$fid = $c->req->param('id');
		$c->log->info("On edite un folder $fid");
		my $folder = folder_par_id($c->req->param('id'));
		if ( $folder ) {
			$c->log->info("Je trouve le folder.");
			$vars->{'folder'} = $folder;
		} else {
			$c->log->error("Folder '$fid' introuvable");
			$fid = undef;
		}
	}
	$vars->{'folders'} = list_folders();

	if ( $c->req->param('submit') ) {
		my $name = $c->req->param('name');
		my $par  = $c->req->param('parent');
		if ( $vars->{'folder'} ) {
			$c->log->info("Mise a jour du folder '$fid' avec name='$name' et parent='$par'");
			set_folder_name($fid, $name);
			set_folder_par($fid, $par);
		} else {
			$c->log->info("Creation du folder '$fid' avec name='$name' et parent='$par'");
			$fid = new_folder($name, $par);
		}
		$c->res->redirect(abs_path('/'));
		return;
	}
	$c->detach('TT','final', [ 'folder_edit', $vars ]);
}

sub delete :Local {
	my ( $self, $c ) = @_;
	if ( ! $c->req->param('id') ) {
		...;
	}
	my $fid = $c->req->param('id');
	$c->log->info("On supprime le folder '$fid'");
	my $folder = folder_par_id($c->req->param('id'));
	if ( ! $folder ) {
		...;
	}
	my $subs = [ folder_par_par($fid) ];
	if ( @$subs ) {
		...;
	}
	my $docs = [ document_par_folder_id($fid) ];
	if ( @$docs ) {
		...;
	}
	del_folder($fid);
	$c->res->redirect(abs_path('/'));
}
sub view :Local {
	my ( $self, $c ) = @_;
	my $vars = {};
	$vars->{'folders'} = list_folders();
	my $fid = $c->req->param('id') // -1;
	if ( $fid == -1 ) {
		$vars->{'folder'} = { 'FOLDER_ID' => -1, 'FOLDER_NAME' => 'Racine' };
	} else {
		$vars->{'folder'} = folder_par_id($fid);
	}
	$vars->{'list_folders'} = [ folder_par_par($fid) ];
	$vars->{'list_documents'} = [ document_par_folder_id($fid) ];
	$c->detach('TT', 'final', [ 'folder_view', $vars ]);
}

1;
