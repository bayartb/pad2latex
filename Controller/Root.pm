package Pad2latex::Controller::Root;

use strict;
use warnings;
use base 'Catalyst::Controller';

use Pad2latex::Sql::Basic;
use Pad2latex::Engine::Auth;
use Pad2latex::Engine::Path;

__PACKAGE__->config->{'namespace'} = '';

sub auto :Private {
	my ( $self, $c ) = @_;
	return check_auth($c);
}

sub default :Path('') {
	my ( $self, $c ) = @_;
	my $vars = {};
	$c->detach('TT','final', [ '404', $vars ]);
}

sub index :Path('') :Args(0) {
	my ( $self, $c ) = @_;
	my $vars = {};
	$vars->{'folders'} = list_folders();
use Data::Dumper;
	$c->log->info(Dumper($vars));
	$c->detach('TT','final', [ 'index', $vars ]);
}

sub login :Local {
	my ( $self, $c ) = @_;

	my $login = $c->req->param('login');
	my $pass  = $c->req->param('password');
	if ( defined $login && defined $pass && $login eq 'toto' && $pass eq 'toto' ) {
		$c->session->{'user_id'} = 42;
		$c->session->{'user_name'} = $login;
		$c->res->redirect(abs_path('/'));
		return;
	}
	my $vars = {};
	$c->detach('TT','final', [ 'login', $vars ]);
}

1;
