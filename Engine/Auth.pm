package Pad2latex::Engine::Auth;

use strict;
use warnings;

use base 'Exporter';
our @EXPORT = qw(
	check_auth
);

use Pad2latex::Engine::Path;

sub check_auth($) {
	my ( $c ) = @_;

	my $path = $c->req->path;
	if ( $path eq 'login' ) {
		$c->log->info("No authent required for the login page.");
		return 1;
	}

	my $sess = $c->session;
	if ( ! defined $sess->{'user_id'} ) {
		$c->log->debug("Authentication required for path '$path'.");
		$c->res->redirect(abs_path('/login'));
		return 0;
	}

	# FIXME Pour le moment, on suppose qu'un user_id dans la session ca suffit.
	return 1;
}


1;
