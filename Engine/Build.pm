package Pad2latex::Engine::Build;

use strict;
use warnings;

use base 'Exporter';
our @EXPORT = qw (
	prepare_build_directory
	prepare_store_directory
	check_build_process
	build_has_tex_log
	build_get_tex_log
	build_get_synth_log
	build_has_my_log
	build_get_my_log
);

use English; # To have $MATCH instead of $&
use FDN::Common::Config;
use FDN::Common::Utils qw(mkdir_p slurp_file);
use Pad2latex::Sql::Basic;
use Pad2latex::Engine::Config;

our $BUILD_DIR;
our $STORE_DIR;

BEGIN {
	$BUILD_DIR = Config_Key($CFG,'dirs','processing');
	if ( ! defined $BUILD_DIR ) {
		die "There must be a directory in $CFG section [dirs] key 'processing'. This is where I will run the LaTeX/Pandoc compilation.";
	}
	if ( ! -d $BUILD_DIR ) {
		mkdir_p($BUILD_DIR);
		if ( ! -d $BUILD_DIR ) {
			die "Directory $BUILD_DIR does not exist, and I failed to create it.";
		}
	}
	if ( ! -w $BUILD_DIR ) {
		die "I can't write in directory $BUILD_DIR. I need to create and remove directories and files from here.";
	}
	$STORE_DIR = Config_Key($CFG,'dirs','storage');
	if ( ! defined $STORE_DIR ) {
		die "There must be a directory in $CFG section [dirs] key 'storage'. This is where I will store my documents.";
	}
	if ( ! -d $STORE_DIR ) {
		mkdir_p($STORE_DIR);
		if ( ! -d $STORE_DIR ) {
			die "Directory $STORE_DIR does not exists. And I failed to create it.";
		}
	}
	if ( ! -w $STORE_DIR ) {
		die "I can't write in directory $STORE_DIR. I need write permissions to store the final documents in that place.";
	}
}

sub prepare_build_directory($) {
	my ( $bid ) = @_;
	my $dir = $BUILD_DIR.'/'.$bid;
	if ( -d $dir && -w $dir ) {
		return $dir;
	}
	mkdir_p($dir);
	if ( -d $dir && -w $dir ) {
		return $dir;
	}
	return undef;
}

sub prepare_store_directory($) {
	my ( $bid ) = @_;
	my $dir = $STORE_DIR.'/builds/'.$bid;
	if ( -d $dir && -w $dir ) {
		return $dir;
	}
	mkdir_p($dir);
	if ( -d $dir && -w $dir ) {
		return $dir;
	}
	return undef;
}

sub prepare_build_directories($) {
	my ( $bid ) = @_;
	mkdir_p($BUILD_DIR.'/'.$bid);
	mkdir_p($STORE_DIR.'/builds/'.$bid);
	if ( -d $BUILD_DIR.'/'.$bid && -d $STORE_DIR.'/builds/'.$bid ) {
		return 1;
	}
	return 0;
}

sub build_has_tex_log($) {
	my ( $bid ) = @_;
	my $dir = prepare_build_directory($bid);
	if ( $dir && -f "$dir/main.log" ) {
		return 1;
	}
	return 0;
}

sub build_get_tex_log($) {
	my ( $bid ) = @_;
	my $dir = prepare_build_directory($bid);
	if ( $dir && -f "$dir/main.log" ) {
		return "<pre>\n".slurp_file("$dir/main.log")."\n</pre>\n";
	}
	return 0;
}

sub build_has_my_log($) {
	my ( $bid ) = @_;
	my $dir = prepare_build_directory($bid);
	if ( $dir && -f "$dir/builder.stdout" ) {
		return 1;
	}
	return 0;
}

sub build_get_my_log($) {
	my ( $bid ) = @_;
	my $dir = prepare_build_directory($bid);
	if ( $dir && -f "$dir/builder.stdout" ) {
		return "<pre>\n".slurp_file("$dir/builder.stdout")."\n</pre>\n";
	}
	return 0;
}

sub build_get_synth_log($) {
	my ( $bid ) = @_;
	my $dir = prepare_build_directory($bid);
	if ( ! $dir || ! -f "$dir/main.log" ) {
		return 'No log file';
	}
	my $log = slurp_file("$dir/main.log");
	$log =~ s/^LaTeX Font Info:.*?on input line \d+\.\s*\n\(.*?\)//gms;
	$log =~ s/^\s+defining Unicode char U+\S+ \(decimal \S+\)\s*\n//gms;
	$log =~ s/Here is how much of TeX's memory you used.*$//gms;
	$log =~ s/^This is .*?\n\*\*main\s*\n//gms;
	my $full = $log;

	my $count = 1;
	my $res;
	while ( $log =~ s/\n\.\/(main|\d+)\.tex:(\d+): .*?\nl.\2 (.*?)\n(.*?)\n//ms ) {
		my ( $pad, $line, $frac1, $frac2 ) = ( $1, $2, $3, $4 );
		$res .= "Error $count:<br>\n";
		$res .= "In pad $pad<br>\n";
		$res .= "Line $line<br>\n";
		$res .= "Here : <span class=\"fail\">$frac1</span> <span class=\"valid\">$frac2</span><br>\n";
		$res .= "<pre>$MATCH</pre>\n";
		$count++;
		last if $count > 25;
	}

	$res .= "\n\nFull log : <br>\n<pre>\n".$full."\n</pre>";
	return $res;
}


sub check_build_process($) {
	my ( $bid ) = @_;
	my $build = build_par_id($bid);
	my $pid = $build->{'BUILD_PROCESS'};
	if ( ! $pid ) {
		set_build_state($bid, 'failed');
		set_build_err($bid, 'Seems no process was ever in charge of building the document. No way it can work. Please consult a wizzard to fix.');
		return 0;
	}
	if ( ! -d "/proc/$pid" ) {
		set_build_state($bid, 'failed');
		set_build_err($bid, "The process which was in charge of building the document died.");
		set_build_process($bid, '0');
		return 0;
	}
	my $exe = readlink("/proc/$pid/exe");
	if ( $exe !~ /perl/ ) {
		# S'pas le bon process
		set_build_state($bid, 'failed');
		set_build_err($bid, "The process which was in charge of building the document died.");
		set_build_process($bid, '0');
		return 0;
	}
	open FILE, "</proc/$pid/cmdline";
	my $line = <FILE>;
	close FILE;
	if ( $line =~ /pad2latex.fcgi/ ) {
		# On n'a pas encore eu le temps d'y reflechir
		return 1;
	}
	if ( $line !~ /pad2latex-builder.*\b$bid\b/ ) {
		# S'pas le bon process
		set_build_state($bid, 'failed');
		set_build_err($bid, "The process which was in charge of building the document is... well... there is a process by that name, but building another document. Something is really fucked up, I guess. Command line is '$line'.");
		set_build_process($bid, 0);
		return 0;
	}
	return 1;
}


1;
