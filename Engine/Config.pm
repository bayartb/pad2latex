package Pad2latex::Engine::Config;

use strict;
use warnings;

use FDN::Common::Config;

use base 'Exporter';
our @EXPORT = qw(
	$CFG
);

our $CFG;
BEGIN {
	$CFG = '/etc/fdn/pad2latex.cfg';
	if ( ! -f $CFG ) {
		die "I need to find my config file in $CFG";
	}
}


1;
