package Pad2latex::Engine::Pad;

use strict;
use warnings;
use base 'Exporter';

use Encode;
use Etherpad;
use URI::Escape;
use FDN::Common::Config;
use FDN::Common::Logger;
use Pad2latex::Sql::Basic;
use Pad2latex::Engine::Config;

our @EXPORT = qw (
	check_url
	fetch_pads

	list_pad_pfs
	pf_name
);

my $cl;
my $pad_name;
my $pad_url;
my $pad_key;

sub list_pad_pfs() {
	my $list = Config_Key($CFG,'pads','@list');
	my $res = [];
	for my $item ( @$list ) {
		my $url = Config_Key($CFG,'etherpad-'.$item,'url');
		my $key = Config_Key($CFG,'etherpad-'.$item,'key');
		my $name = Config_Key($CFG,'etherpad-'.$item,'name');
		if ( ! $url || ! $key || ! $name ) {
			die "Invalid etherpad instance '$item'";
		}
		push @$res, {
			'name' => $name.' ('.$url.')',
			'value' => $item
		};
	}
	return $res;
}
BEGIN {
	my $tmp = list_pad_pfs();
}

sub pf_name($) {
	my ( $doc ) = @_;
	my $name = $doc->{'DOCUMENT_PF'};
	return Config_Key($CFG, 'etherpad-'.$name, 'name') // 'MISSING';
}

sub check_cnx($) {
	my ( $name ) = @_;
	if ( $name ne $pad_name ) {
		$pad_name = $name;
		$pad_url = Config_Key($CFG,'etherpad-'.$name,'url');
		$pad_key = Config_Key($CFG,'etherpad-'.$name,'key');
		$cl = undef;
	}
	if ( ! defined $cl ) {
		$cl = Etherpad->new(url => $pad_url, apikey => $pad_key);
	}
	if ( $cl->check_token ) {
		return 1;
	}
	return 0;
}

sub clean_pad_id($;$) {
	my ( $url, $test ) = @_;
	my $pad_id = $url;
	if ( $pad_id !~ s/^\Q$pad_url\E\/?// ) {
		if ( $test ) {
			$test->{'valid_site'} = 0;
		}
		return undef;
	}
	if ( $test ) {
		$test->{'valid_site'} = 1;
		# Default for valid_position
		$test->{'valid_position'} = 1;
	}
	if ( $pad_id =~ s/^group.html\/\d+\/pad.html\/// ) {
		# Pad is from ep_team_work, remove group info
		return $pad_id;
	} elsif ( $pad_id =~ s/^p\/// ) {
		# Pad in a public adress of the pad
		return $pad_id;
	}
	if ( $test ) {
		$test->{'valid_position'} = 0;
	}
	return undef;
}

sub read_pad($;$$) {
	my ( $pad_id, $test, $doc ) = @_;
	my $text = $cl->get_text($pad_id);
	if ( ! defined $text ) {
		print STDERR "Failed to fetch '$pad_id'\n";
		if ( $pad_id =~ /%/ ) {
			$pad_id = uri_unescape($pad_id);
			my $utf = 0;
			if ( $pad_id =~ /�/ ) {
				$pad_id = decode('UTF-8',$pad_id);
				$utf = 1;
			}
			$text = $cl->get_text($pad_id);
			
			if ( defined $text && defined $doc ) {
				my $did = $doc->{'DOCUMENT_ID'};
				my $pad = $doc->{'DOCUMENT_PAD'};
				$pad = uri_unescape($pad);
				if ( $utf ) {
					$pad = decode('UTF-8',$pad);
				}
				set_document_pad($did,$pad);
				if ( $test ) {
					$test->{'fix_pad_escape'} = 1;
				}
			}
		}
	}
	return $text;
}

sub fetch_pad($$;$) {
	my ( $ctx, $pad_id, $doc ) = @_;
	my $text = read_pad($pad_id, $ctx->{'test'}, $doc);
	if ( ! defined $text ) {
		if ( $ctx->{'test'} ) {
			$ctx->{'test'}->{'pad_access'} = 0
		}
		return 0;
	}
	if ( $ctx->{'test'} ) {
		$ctx->{'test'}->{'pad_access'} = 1;
	}
	my $ret = 1;
	my $res = '';
	my @lines = split /\n/, $text;
	my $count = 0;
	while ( @lines ) {
		my $line = shift @lines;
		$count += 1;
		if ( $line !~ /^\s*#(\w+)(?:\s+(.*))?$/ ) {
			$res .= $line."\n";
			next;
		}

		my $directive = $1;
		my $args = $2;
		if ( $directive eq 'INCLUDE' ) {
			my $inpad_id = clean_pad_id($args);
			if ( ! $inpad_id ) {
				# Invalid directive.
				$ret = 0;
				if ( $ctx->{'test'} ) {
					push @{$ctx->{'test'}->{'errors'}}, {
						pad => $pad_id,
						line_num => $count,
						line => $line,
						msg => 'Invalid pad URL in #INCLUDE directive.'
					};
				}
				next;
			}
			$inpad_id =~ s/^\s*//;
			$inpad_id =~ s/\s*$//;
			if ( $ctx->{'seen'}->{$inpad_id} ) {
				$res .= '\input '.$ctx->{'seen'}->{$inpad_id}."\n";
				next;
			}
			$ctx->{'seen'}->{$inpad_id} = $ctx->{'count'}++;
			if ( ! fetch_pad($ctx, $inpad_id) ) {
				$ret =0;
				if ( $ctx->{'test'} ) {
					push @{$ctx->{'test'}->{'errors'}}, {
						pad => $pad_id,
						line_num => $count,
						line => $line,
						msg => 'Failed to include that pad ('.$inpad_id.')'
					};
				}
				next;
			}
			$res .= '\input '.$ctx->{'seen'}->{$inpad_id}."\n";
			next;
		}
		# Unknown directive
		push @{$ctx->{'test'}->{'errors'}}, {
			pad => $pad_id,
			line_num => $count,
			line => $line,
			msg => "Unknown directive '$directive'"
		};
	}

	if ( ! $ctx->{'test'} && $ctx->{'ext'} && $ctx->{'dir'} ) {
		my $name = $ctx->{'seen'}->{$pad_id};
		my $ext  = $ctx->{'ext'};
		my $dir  = $ctx->{'dir'};
		my $filename = "$dir/$name.$ext";

		open FILE, ">$filename";
		print FILE $res;
		close FILE;
	}
	return $ret;
}

sub check_url($$) {
	my ( $doc, $test ) = @_;

	if ( ! check_cnx($doc->{'DOCUMENT_PF'}) ) {
		$test->{'valid_token'} = 0;
		return;
	}
	my $pad_id = $doc->{'DOCUMENT_PAD'};

	$pad_id = clean_pad_id($pad_id, $test);

	$test->{'valid_token'} = 1;

	my $ctx = {};
	$ctx->{'test'} = $test;
	$ctx->{'count'} = 1;
	$ctx->{'seen'} = { $pad_id => 'main' };
	fetch_pad($ctx, $pad_id, $doc);
}

sub fetch_pads($$) {
	my ( $doc, $dir ) = @_;
	if ( ! check_cnx($doc->{'DOCUMENT_PF'}) ) {
		return 0;
	}
	my $pad_id = clean_pad_id($doc->{'DOCUMENT_PAD'});
	my $ext = 'INVALID';
	if ( $doc->{'DOCUMENT_TYPE'} eq 'md' ) {
		$ext = 'md';
	} elsif ( $doc->{'DOCUMENT_TYPE'} eq 'latex') {
		$ext = 'tex';
	} else {
		...;
	}
	my $ctx = {
		seen => {},
		count => 1,
		ext => $ext,
		dir => $dir
	};
	$ctx->{'seen'}->{$pad_id} = 'main';
	return fetch_pad($ctx, $pad_id);
}


1;
