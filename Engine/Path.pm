package Pad2latex::Engine::Path;

use strict;
use warnings;

use FDN::Common::Config;
use Pad2latex::Engine::Config;

use base 'Exporter';
our @EXPORT = qw(
	abs_path
	rel_path
);

our $URL;
our $PATH;
BEGIN {
	my $url = Config_Key($CFG,'Catalyst','url');
	if ( ! $url ) {
		die "In config file '$CFG' there must be a [Catalyst] section with an 'url' key. ".
			"It should be the url of pad2latex as installed on the sever. Eg: https://stuff.example.com/pad2latex";
	}
	if ( $url !~ /^https?:\/\/[^\/]+\// ) {
		die "The url '$url' in file '$CFG'/[Catalyst] is weird.";
	}
	$URL = $url;

	# If there is no path provided, it is taken from the URL.
	my $path = Config_Key($CFG,'Catalyst','path');
	if ( ! $path ) {
		$path = $url;
		$path =~ s/^https?:\/\/[^\/]+\///;
	}
	if ( $path !~ /^\// ) {
		# We ensure path is absolute (thus, begins with /)
		$path = '/'.$path;
	}
	if ( $path !~ /\/$/ ) {
		# We ensure path is ending with a /
		$path .= '/';
	}
	$PATH = $path;
}

sub abs_path {
	my ( $path ) = @_;
	$path =~ s/^\///;
	return $PATH.$path;
}

1;
