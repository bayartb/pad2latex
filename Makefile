

DESTDIR?=/
CONFDIR=$(DESTDIR)/etc/fdn
SQLSQL=$(DESTDIR)/usr/share/fdn/sql/pad2latex
FCGIDIR=$(DESTDIR)/usr/share/fcgi-bin
PERLDIR=$(DESTDIR)/usr/share/perl5/Pad2latex
HTMLDIR=$(DESTDIR)/usr/share/pad2latex/templates/html
CSSDIR=$(DESTDIR)/var/www/pad2latex/css
STATICDIR=$(DESTDIR)/var/www/pad2latex/static
JSDIR=$(DESTDIR)/usr/share/javascript/pad2latex
BINDIR=$(DESTDIR)/usr/local/bin

install:
	@install --owner root --group fdnconf --mode 644 config/*.cfg $(CONFDIR)
	@install --directory --owner root --group fdnconf --mode 2755 $(CONFDIR)/pad2latex
	@install --owner root --group root --mode 0644 config/logger.conf $(CONFDIR)/pad2latex
	@mkdir -p $(SQLSQL)
	@install --owner root --group root --mode 0644 sql/*.sql $(SQLSQL)
	@install --directory --owner root --group fdnconf --mode 2755 $(DESTDIR)/etc/fdn/check
	@install --owner root --group root --mode 0644 config/check.cfg $(DESTDIR)/etc/fdn/check/pad2latex.cfg
	@install --directory --owner root --group root --mode 0755 $(FCGIDIR)
	@install --owner www-data --group www-data --mode 0755 fcgi/*.fcgi $(FCGIDIR)
	@install --owner root --group root --mode 0755 bin/*.pl $(BINDIR)
	@mkdir -p $(PERLDIR)
	@install --owner root --group root --mode 644 *.pm $(PERLDIR)/..
	@mkdir -p $(PERLDIR)/Controller
	@install --owner root --group root --mode 644 Controller/*.pm $(PERLDIR)/Controller	
	@mkdir -p $(PERLDIR)/Engine
	@install --owner root --group root --mode 644 Engine/*.pm $(PERLDIR)/Engine	
	@mkdir -p $(PERLDIR)/View
	@install --owner root --group root --mode 644 View/*.pm $(PERLDIR)/View	
	@mkdir -p $(PERLDIR)/Sql
	@install --owner root --group root --mode 644 Sql/*.pm $(PERLDIR)/Sql	
	@mkdir -p $(HTMLDIR)
	@install --owner root --group root --mode 644 templates/*.html $(HTMLDIR)
	@mkdir -p $(CSSDIR)
	@install --owner root --group root --mode 644 css/*.css $(CSSDIR)
	@mkdir -p $(JSDIR)
	@install --owner root --group root --mode 644 js/*.js $(JSDIR)
	@mkdir -p $(STATICDIR)
	@install --owner root --group root --mode 0644 static/*.svg $(STATICDIR)

db: install
	@check-db -f check/pad2latex.cfg -c pad2latex --run

