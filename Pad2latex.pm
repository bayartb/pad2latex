package Pad2latex;

use strict;
use warnings;
use FDN::Common::Logger qw();
use Catalyst::Runtime;
use Log::Log4perl::Catalyst;
use base 'Catalyst';
use Catalyst qw/
	-Debug
	Unicode::Encoding
	Session
	Session::Store::FastMmap
	Session::State::Cookie
/;

__PACKAGE__->config(
	name => 'Pad2latex',
	'View::TT' => {
		'TEMPLATE_EXTENSION' => '.html',
		'INCLUDE_PATH' => [ '/usr/share/pad2latex/templates/html' ],
		#'PRE_PROCESS' => 'lang.fr.html',
		'PROVIDERS' => [ {
			'name' => 'Encoding',
			'copy_config' => [ 'INCLUDE_PATH' ]
		} ],
		'render_die' => 1
	}
);

__PACKAGE__->log(Log::Log4perl::Catalyst->new('/etc/fdn/pad2latex/logger.conf', autoflush => 1));

__PACKAGE__->setup();

sub my_info {
	my ( $msg ) = @_;
	print STDERR $msg."\n";
	warn $msg;
	__PACKAGE__->log->info($msg);
}

*FDN::Common::Logger::debug = *my_info;
*FDN::Common::Logger::info = *my_info;

1;
