# pad2latex

Basic tools allowing to process a pad as a LaTeX source to produce a PDF document.
Will be extended.

It should run smoothly with any instance of etherpad. Any feedback is welcome, here
or elsewhere. The application is a fastcgi server, written in perl, using the
framework `Catalyst`. It heavily uses the fdn-libs for Sql, for config files, and
for a few utils. The sources of those libraries are available on my own computer,
at http://edgard.fdn.fr/developpements/fdnlibs/index.shtml It will be made available
on a public git soon (it is only on a private git for now).


