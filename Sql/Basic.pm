package Pad2latex::Sql::Basic;

use strict;
use warnings;

use base 'Exporter';

our @EXPORT = qw (
	set_document_dtin
	set_document_dtmod
	document_recent
	build_recent

	set_build_dtin
	set_build_dtmod
	set_build_dtstart
	build_par_document_id_sorted

	tous_folder
	list_folders
);

use FDN::Sql::Basic;
use FDN::Sql::Basic2;
use Pad2latex::Sql::DBC;

minimal_functions({
	'FOLDER' => [ 'NAME', 'PAR' ],
	'DOCUMENT' => [ 'NAME', 'FOLDER_ID', 'PF', 'PAD' ],
	'BUILD' => [ 'DOCUMENT_ID', 'USER' ]
});
update_functions([
	'FOLDER' => 'NAME',
	'FOLDER' => 'PAR',
	'DOCUMENT' => 'NAME',
	'DOCUMENT' => 'FOLDER_ID',
	'DOCUMENT' => 'PF',
	'DOCUMENT' => 'PAD',
	'DOCUMENT' => 'TYPE',
	'DOCUMENT' => 'STATE',
	'BUILD' => 'STATE',
	'BUILD' => 'ERR',
	'BUILD' => 'WARN',
	'BUILD' => 'PROCESS',
]);
search_functions([
	'DOCUMENT' => [ 'FOLDER_ID' ],
	'FOLDER'   => [ 'PAR' ],
	'BUILD'	   => [ 'DOCUMENT_ID' ]
]);

sub set_document_dtin($$) {
	my ( $did, $date ) = @_;
	return __set_tbl_val($dbh, 'DOCUMENT', 'DOCUMENT_DTIN', $did, dodate($date));
}
sub set_document_dtmod($$) {
	my ( $did, $date ) = @_;
	return __set_tbl_val($dbh, 'DOCUMENT', 'DOCUMENT_DTMOD', $did, dodate($date));
}
sub document_recent {
	return __par_champ_ext($dbh, 'DOCUMENT', undef, {
		order => 'down:DOCUMENT_DTMOD',
		range => '0+25'
	});
}
sub build_recent {
	return __par_champ_ext($dbh, 'BUILD', undef, {
		order => 'down:BUILD_ID',
		range => '0+25'
	});
}

sub set_build_dtin($$) {
	my ( $did, $date ) = @_;
	return __set_tbl_val($dbh, 'BUILD', 'BUILD_DTIN', $did, dodate($date));
}
sub set_build_dtmod($$) {
	my ( $did, $date ) = @_;
	return __set_tbl_val($dbh, 'BUILD', 'BUILD_DTMOD', $did, dodate($date));
}
sub set_build_dtstart($$) {
	my ( $did, $date ) = @_;
	return __set_tbl_val($dbh, 'BUILD', 'BUILD_DTSTART', $did, dodate($date));
}
sub build_par_document_id_sorted($) {
	my ( $did ) = @_;
	return __par_champ_ext($dbh, 'BUILD', [ [ 'BUILD.DOCUMENT_ID', '=', $did ] ],
		{ order => 'down:BUILD_DTMOD' } );
}

sub tous_folder {
	return __tous($dbh,'FOLDER');
}

sub list_folders {
	my $res = [];
	my $hash = { };
	my $tous = [ tous_folder($dbh) ];
	for my $folder ( @$tous ) {
		my $par_id = $folder->{'FOLDER_PAR'};
		if ( $par_id == -1 ) {
			push @$res, $folder;
		}
		my $id = $folder->{'FOLDER_ID'};
		if ( exists $hash->{$id} ) {
			$folder->{'contains'} = $hash->{$id}->{'contains'};
		}
		$hash->{$id} = $folder;
		if ( ! exists $hash->{$par_id} ) {
			$hash->{$par_id} = { contains => [] };
		}
		push @{$hash->{$par_id}->{'contains'}}, $folder;
	}

	$res = [ sort { $a->{'FOLDER_NAME'} cmp $b->{'FOLDER_NAME'} } @$res ];
	return $res;
}

1;
