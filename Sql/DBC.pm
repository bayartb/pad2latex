package Pad2latex::Sql::DBC;

use strict;
use warnings;

use base 'Exporter';
use FDN::Sql::DBC;

our @EXPORT = qw( $dbh );

our $dbh;
BEGIN { $dbh = do_connect('pad2latex'); }
END { do_disconnect($dbh); }

1;
