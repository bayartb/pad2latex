package Pad2latex::View::TT;

use strict;
use warnings;
use base 'Catalyst::View::TT';
use Encode;
use Carp ();

sub partial {
	my ( $self, $c, $tpl, $vars ) = @_;
	if ( !defined $tpl ) {
		Carp::confess "I need a template to render.";
	}

	if ( $tpl !~ /\.html$/ ) {
		$tpl = $tpl.'.html';
	}
#	$vars->{'project'} = 1 if $c->session->{'project'};
#	$vars->{'user'} = $c->stash->{'user'} if exists $c->stash->{'user'};
#	$vars->{'lang'} = $c->session->{'lang'};

	$vars->{'path_prefix'} = $Pad2latex::Engine::Path::PATH;

	my $res = $c->view('TT')->render($c, $tpl, $vars);
	if (UNIVERSAL::isa($res, 'Template::Exception')) {
		my $error = qq/Couldn't render template "$res"/;
		$c->log->error($error);
		$c->error($error);
		return '';
	}
	Encode::_utf8_on($res);
	return $res;
}

sub final {
	my ( $self, $c, $tpl, $vars ) = @_;
	my $res = $c->forward($self, 'partial', [ $tpl, $vars ]);
	$c->response->body($res);
	$c->response->content_type('text/html; charset=utf-8');
	$c->response->headers->header('Expires' => '-1');
}

1;
