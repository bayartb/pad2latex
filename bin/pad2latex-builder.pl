#!/usr/bin/perl

use strict;
use warnings;

use FDN::Common::Utils qw(mkdir_p slurp_file);
use Pad2latex::Sql::Basic;
use Pad2latex::Engine::Build;

my $bid = $ARGV[0];
print STDERR "Bid is $bid\n";
if ( $bid !~ /^\d+$/ ) {
	exit (1);
}
my $dir = prepare_build_directory($bid);
print STDERR "Writing to $dir/builder.stdout\n";
open OUT, ">$dir/builder.stdout";

close STDIN;
close STDOUT;
close STDERR;

print OUT "I am trying to run the build number $bid\n";

print OUT "Changing to directory $dir\n";
chdir($dir);

print OUT "Running pdflatex\n";
`pdflatex -interaction batchmode -file-line-error main`;
if ( $? ) {
	print OUT "Probably a failure, return status : $?\n";
}

if ( -f 'main.toc' || -f 'main.lof' || -f 'main.lot' ) {
	print OUT "\n\n";
	print OUT "I found a table of contents, running pdflatex again\n";
	`pdflatex -interaction batchmode -file-line-error main`;
	if ( $? ) {
		print OUT "Probably a failure, return status : $?\n";
	}
}

my $log = slurp_file('main.log');
if ( $log =~ /Rerun to get cross-references right\./ms ) {
	print OUT "\n\n";
	print OUT "LaTeX claims we need another run to get the references right.\n";
	`pdflatex -interaction batchmode -file-line-error main`;
	if ( $? ) {
		print OUT "Probably a failure, return status : $?\n";
	}
}

$log = slurp_file('main.log');
if ( $log =~ /Rerun to get cross-references right\./ms ) {
	print OUT "\n\n";
	print OUT "LaTeX claims we need still another run to get the references right.\n";
	`pdflatex -interaction batchmode -file-line-error main`;
	if ( $? ) {
		print OUT "Probably a failure, return status : $?\n";
	}
}

$log = slurp_file('main.log');
my $warn = 0;
my $warns = [];
if ( $log =~ /LaTeX Warning: There were undefined references./ms ) {
	print OUT "\n\n";
	print OUT "LaTeX claims there were undefined references.\n";
	$warn = 1;
	push @$warns, "Undefined references:";
	my $tmp = $log;
	while ( $tmp =~ s/^.*?(LaTeX Warning: Reference `.*?' on page \S+ undefined on input line \d+.)//ms ) {
		my $warning = $1;
		print OUT "Added warning $warning\n";
		push @$warns, $warning;
	}
	print OUT "No more warnings\n";
}

my $tmp = $log;
my $nberr = 0;
while ( $tmp =~ s/\n\.\/(main|\d+)\.tex:(\d+): .*?\nl.\2 (.*?)\n(.*?)\n//ms ) {
	$nberr += 1;
	$warn = 1;
}
if ( $nberr ) {
	push @$warns, "LaTeX found $nberr errors. Some may be serious, please check.";
}

print OUT "\n\n";
print OUT "Finished running pdflatex\n";
if ( -f "$dir/main.pdf" ) {
	print OUT "A pdf file is available, will consider it a success\n";
	if ( $warn ) {
		print OUT "There are serious warnings\n";
		set_build_state($bid,'warning');
		set_build_warn($bid, join ("\n", @$warns) );
	} else {
		set_build_state($bid,'success');
	}
	my $dir2 = prepare_store_directory($bid);
	`cp $dir/main.pdf $dir2/main.pdf`;
} else {
	print OUT "No pdf file is available, will consider it a failure\n";
	set_build_state($bid,'failed');
	set_build_err($bid,'I found no pdf file after running the LaTeX compiler. Please check the TeX log file.');
}
set_build_process($bid,0);
exit(0);

