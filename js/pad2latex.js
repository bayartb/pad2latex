(function () {
"use strict";

var path_prefix;
function toggle_password(elem) {
	var $elem = $(elem);
	var $input = $elem.closest('form').find('input[name=password]');
	if ( $input.attr('type') === 'password' ) {
		$input.attr('type','text');
	} else {
		$input.attr('type','password');
	}
}

function set_path_prefix(prefix) {
	path_prefix = prefix;
}

function view_folder(id) {
	location=path_prefix + 'folder/view?id=' + id;
}

function view_document(id) {
	location=path_prefix + 'document/view?id=' + id;
}

function view_build(id) {
	location=path_prefix + 'build/view?id=' + id;
}
function view_build_tex_log(id) {
	location=path_prefix + 'build/view_tex_log?id=' + id;
}
function view_build_synth_log(id) {
	location=path_prefix + 'build/view_synth_log?id=' + id;
}
function view_build_my_log(id) {
	location=path_prefix + 'build/view_my_log?id=' + id;
}

window.pad2latex = {};
pad2latex.toggle_password 	= toggle_password;
pad2latex.view_folder		= view_folder;
pad2latex.view_document		= view_document;
pad2latex.view_build		= view_build;
pad2latex.view_build_tex_log	= view_build_tex_log;
pad2latex.view_build_synth_log	= view_build_synth_log;
pad2latex.view_build_my_log	= view_build_my_log;
pad2latex.set_path_prefix	= set_path_prefix;

})();

var pad2latex = window.pad2latex;
