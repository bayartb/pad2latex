
drop table if exists DOCUMENT;
create table DOCUMENT (
	DOCUMENT_ID bigint(20) not null default '0',

	FOLDER_ID bigint(20) not null default '-1',
	DOCUMENT_NAME varchar(50),
	-- open : to be displayed in the list
	-- close : to be displayed on demand
	-- del : never displayed
	DOCUMENT_STATE varchar(20),
	-- latex : the document is to be processed directly with PDFLaTeX
	-- md : the document is in markdown, to be processed with pandoc
	DOCUMENT_TYPE varchar(20),
	-- the name of the etherpad instance (as in config @list)
	DOCUMENT_PF varchar(20),
	-- url to the main pad
	DOCUMENT_PAD varchar(300),

	DOCUMENT_DTIN datetime,
	DOCUMENT_DTMOD datetime,
	primary key(DOCUMENT_ID),
	index(FOLDER_ID),
	index(DOCUMENT_DTMOD)
)
ENGINE=MyISAM default charset=utf8;
replace into SEQ values('DOCUMENT_ID',0,0,99999999,1,'0');

drop table if exists BUILD;
create table BUILD (
	BUILD_ID bigint(20) not null default '0',

	DOCUMENT_ID bigint(20) not null default '0',
	-- new : compilation demandee
	-- data : donnees extraites et rangees (ou ?)
	-- building : compilation en cours (depuis quand ? quel process ?)
	-- canceled : compilation tuee a la main
	-- failed : echec de compilation (ou sont les logs ? comment les analyser ?)
	-- success : compilation reussiee (ou est le PDF ? ou sont les logs ? comment les analyser ?)
	BUILD_STATE varchar(20),
	-- Quand le state est 'failed' on note ici la cause principale de l'echec.
	BUILD_ERR text,
	-- Quand le state est 'warning' on not ici la cause principale de l'echec.
	BUILD_WARN text,
	-- qui a demande ce build ?
	BUILD_USER varchar(50),
	-- date de la demande
	BUILD_DTIN datetime,
	-- derniere demande
	BUILD_DTMOD datetime,
	-- lancement de la compilation
	BUILD_DTSTART datetime,
	-- le pid du process
	BUILD_PROCESS bigint(10),

	primary key(BUILD_ID),
	index(DOCUMENT_ID,BUILD_DTMOD),
	index(BUILD_DTMOD)
)
ENGINE=MyISAM default charset=utf8;
replace into SEQ values('BUILD_ID',0,0,99999999,1,'0');

drop table if exists FOLDER;
create table FOLDER (
	FOLDER_ID bigint(20) not null default '0',

	FOLDER_NAME varchar(50),
	FOLDER_PAR bigint(20) not null default '-1',
	primary key(FOLDER_ID)
)
ENGINE=MyISAM default charset=utf8;
replace into SEQ values('FOLDER_ID',0,0,99999999,1,'0');

